import React, {lazy, Suspense} from 'react';
import {Route, Switch} from 'react-router-dom';
import Home from "../pages/Home/Home";
import Loader from "../components/Loader/Loader";

const Favorite = lazy(() => import("../pages/Favorite/Favorite"));
const Cart = lazy(() => import("../pages/Cart/Cart"));

const AppRouter = () => (
        <main className={"main"}>
          <Suspense fallback={<Loader/>}>
            <Switch>
              <Route exact path={"/"} component={Home}/>
              <Route path={"/favorite"} component={Favorite}/>
              <Route path={"/cart"} component={Cart}/>
            </Switch>
          </Suspense>
        </main>
    );

export default AppRouter;