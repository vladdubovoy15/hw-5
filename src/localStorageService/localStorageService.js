export const getItem = key => JSON.parse(localStorage.getItem(key));
export const saveItem = (key, item) => localStorage.setItem(key, JSON.stringify(item));
const clearItem = (key) => localStorage.removeItem(key);

const localStorageService = {clearItem, saveItem, getItem}
export default localStorageService;