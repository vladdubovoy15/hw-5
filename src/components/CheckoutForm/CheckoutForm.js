import React, {memo} from 'react';
import {Form, withFormik} from 'formik';
import * as Yup from "yup";
import {connect} from "react-redux";
import localStorageService from "../../localStorageService/localStorageService";
import {cartOperations} from "../../store/cart";
import CheckoutField from '../CheckoutField/CheckoutField';
import classes from './CheckoutForm.module.scss';

const schema = Yup.object().shape({
  firstName: Yup.string().required("Name field can't be blank"),
  lastName: Yup.string().required("Surname field can't be blank"),
  age: Yup.number().min(14, "Your age must be at least 14 y.o").required("Age field can't be blank"),
  address: Yup.string().required("Address field can't be blank"),
  mobile: Yup.string().required("Mobile number can't be blank"),
})

const CheckoutForm = () => {
  return (
      <div className={classes.wrapper}>
        <h3 className={`center red-text title ${classes.title}`}>Checkout form</h3>
        <Form className={"col s6"}>
          <CheckoutField name={"firstName"} type="text" label={"First name"} placeholder={"Enter your first name"}/>
          <CheckoutField name={"lastName"} type="text" label={"Last name"} placeholder={"Enter your second name"}/>
          <CheckoutField name={"age"} type="number" label={"Age"} placeholder={"Enter your age"}/>
          <CheckoutField name={"address"} type="text" label={"Address"} placeholder={"Shipment address"}/>
          <CheckoutField name={"mobile"} type="tel" label={"Mobile number"} placeholder={"Enter your phone number"}/>
          <button type={"submit"} className={'btn waves-effect waves-light red'}>
            Checkout
            <i className="material-icons right">send</i>
          </button>
        </Form>
      </div>
  )
}

const checkoutFormWithFormik = withFormik({
  mapPropsToValues: () => ({
    firstName: '',
    lastName: '',
    age: '',
    address: '',
    mobile: '',
  }),
  validationSchema: schema,
  handleSubmit: (values, formik) => {
    const {dispatch} = formik.props;
    console.log(values)
    localStorageService.clearItem("cart");
    dispatch(cartOperations.clearCart());
  }
})(CheckoutForm);

export default memo(connect()(checkoutFormWithFormik));