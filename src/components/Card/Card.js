import React, {memo, useCallback} from 'react';
import './Card.scss';
import Icon from "../Icon/Icon";
import PropTypes from 'prop-types';
import {useDispatch, useSelector} from "react-redux";
import {cartOperations, cartSelectors} from "../../store/cart";
import {favoriteOperations, favoriteSelectors} from '../../store/favorite/index';
import {appOperations} from "../../store/app";

const Card = ({card, trashCan}) => {
  const {title, price, color, img, code, id} = card;
  const favorite = useSelector(favoriteSelectors.getFavorite());
  const cart = useSelector(cartSelectors.getCart());
  const dispatch = useDispatch();

  const addToCartHandler = useCallback(() => {
    if (!cart.find(product => product.id === id)) {
      dispatch(cartOperations.addItemToCart(card))
      dispatch(appOperations.setModal({isOpen: true, header: "Are you sure you want to add to the cart?"}))
    }
  }, [card, id, cart, dispatch])

  const removeFromCartHandler = useCallback(() => {
    dispatch(cartOperations.removeItemFromCart(card))
    dispatch(appOperations.setModal({isOpen: true, header: "Are you sure you want to remove this product?"}))
  }, [dispatch, card])

  const toggleFavorite = useCallback(() => {
    dispatch(favoriteOperations.setFavorite(card))
  }, [dispatch, card])

  return (
      <li className='card z-depth-2'>
        <h3 className='card__title'>{title}</h3>
        <img src={img}
             className={"card__image"}
             alt={title}
             width={200}
             height={240}/>
        <ul>
          <li>code: {code}</li>
          <li>color: {color}</li>
          <li>price: {price}</li>
        </ul>
        <button className={"card__button red waves-effect waves-light"}
                onClick={addToCartHandler}>Add to cart
        </button>
        <Icon filled={Boolean(favorite.find(item => item.id === id))}
              className={"favorite"}
              card={card}
              onClick={toggleFavorite}
        />
        {trashCan && <i className="material-icons trash-can"
                        onClick={removeFromCartHandler}>delete</i>
        }
      </li>
  )
};

Card.propTypes = {
  card: PropTypes.object,
  modalHandler: PropTypes.func,
  favoriteHandler: PropTypes.func,
  favorite: PropTypes.arrayOf(PropTypes.object),
  deleteHandler: PropTypes.func,
}

Card.defaultProp = {
  trashCan: false
}

export default memo(Card);