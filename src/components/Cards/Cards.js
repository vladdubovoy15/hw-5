import React, {memo} from 'react';
import Card from "../Card/Card";
import classes from './Cards.module.scss';

const Cards = ({cardsList, isTrashCanPresent}) => (
    <ul className={classes.wrapper}>
      {cardsList.map(card => <Card key={card.id} card={card} trashCan={isTrashCanPresent}/>)}
    </ul>
);

export default memo(Cards);