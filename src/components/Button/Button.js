import React, {memo, useCallback} from 'react';
import '../../style/style.scss';
import PropTypes from 'prop-types';
import {useDispatch} from "react-redux";
import {appOperations} from "../../store/app";
import {cartOperations} from "../../store/cart";

const Button = ({text, backgroundColor, confirm}) => {
  const dispatch = useDispatch();

  const closeModal = useCallback(() => {
    dispatch(appOperations.setModal({isOpen: false, header: null}))
    dispatch(cartOperations.setCart(confirm))
  }, [confirm, dispatch])

  return (
      <button
          style={{backgroundColor}}
          onClick={closeModal}
          className={"button"}
      >
        {text}
      </button>
  )
}

Button.propTypes = {
  text: PropTypes.string,
  backgroundColor: PropTypes.string,
  clickHandler: PropTypes.func,
  cancel: PropTypes.bool
}

Button.defaultProp = {
  cancel: false
}

export default memo(Button);