import React, {memo} from 'react';

const Footer = () => (
    <footer className="page-footer">
       <span className="footer-copyright">
          © 2021 Copyright Text
      </span>
    </footer>
);

export default memo(Footer);