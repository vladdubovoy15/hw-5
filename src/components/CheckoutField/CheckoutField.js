import React, {memo} from 'react';
import {useField} from "formik";

const CheckoutField = ({name, label, ...rest}) => {
  const [field, fieldState] = useField(name);

  return (
      <div className="row">
        <label className={"col s12"}>{label}
          <input {...field} {...rest} className={fieldState.error && fieldState.touched && "invalid"}/>
          {fieldState.error && fieldState.touched && <span className={"red-text"}>{fieldState.error}</span>}
        </label>
      </div>
  );
};

export default memo(CheckoutField);