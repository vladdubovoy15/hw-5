import React, {memo, useCallback} from 'react';
import classes from './Modal.module.scss';
import '../../style/style.scss';
import PropTypes from 'prop-types';
import {useDispatch} from "react-redux";
import Button from "../Button/Button";
import {cartOperations} from '../../store/cart/';
import {appOperations} from "../../store/app";

const Modal = ({header}) => {
  const dispatch = useDispatch();

  const closeModal = useCallback(() => {
    dispatch(cartOperations.setCartFromLocalStorage());
    dispatch(appOperations.setModal({header: null, isOpen: false}))
  }, [dispatch]);

  return (
      <div className={classes["modal-overlay"]} onClick={closeModal}>
        <div className={classes.wrapper} onClick={(e) => e.stopPropagation()}>
          <header className={classes.header}>
            <h2 className={classes.title}>{header}</h2>
            <button className={classes["close-button"]} onClick={closeModal}>X
            </button>
          </header>
          <div className={classes["action-wrapper"]}>
            <Button backgroundColor={"#b3382c"}
                    text={"Ok"}
                    confirm={true}
            />
            <Button backgroundColor={"#b3382c"}
                    text={"Cancel"}
            />
          </div>
        </div>
      </div>)
};

Modal.propTypes = {
  header: PropTypes.string,
  modalHandler: PropTypes.func,
  cancel: PropTypes.bool,
  actions: PropTypes.element,
}

export default memo(Modal);