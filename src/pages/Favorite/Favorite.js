import React, {memo} from 'react';
import {useSelector} from "react-redux";
import {favoriteSelectors} from "../../store/favorite";
import Cards from "../../components/Cards/Cards";

const Favorite = () => {
  const favorite = useSelector(favoriteSelectors.getFavorite());

  return (
      <>
        {favorite.length
            ? <Cards cardsList={favorite}/>
            : <h3 className={'red-text center'}>Oops! There are no items in the favorite list</h3>
        }
      </>
  );
}

export default memo(Favorite);