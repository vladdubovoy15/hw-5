import React, {memo, useEffect} from 'react';
import Loader from '../../components/Loader/Loader';
import {useDispatch, useSelector} from "react-redux";
import {appOperations, appSelectors} from "../../store/app";
import Cards from "../../components/Cards/Cards";

const Home = () => {
  const dispatch = useDispatch();
  const cards = useSelector(appSelectors.getCards());
  const isLoading = useSelector(appSelectors.getIsLoading());

  useEffect(() => {
    cards.length === 0
        ? dispatch(appOperations.fetchCards())
        : dispatch(appOperations.setLoader(false));
  }, [cards, dispatch])

  return (
      <>
        {isLoading
            ? <Loader/>
            : <Cards cardsList={cards}/>
        }
      </>
  );
};

export default memo(Home);