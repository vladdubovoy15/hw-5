import React, {memo} from 'react';
import {useSelector} from "react-redux";
import {cartSelectors} from "../../store/cart";
import CheckoutForm from '../../components/CheckoutForm/CheckoutForm';
import classes from './Cart.module.scss';
import Cards from "../../components/Cards/Cards";
import {useHistory} from "react-router-dom";

const Cart = () => {
  const cart = useSelector(cartSelectors.getCart());
  const history = useHistory();

  return (
      <div className={classes.cart}>
        {cart.length
            ? <>
                <Cards cardsList={cart} isTrashCanPresent={true}/>
                <CheckoutForm/>
              </>
            : <div className={classes.empty}>
                <h3 className={classes['empty-title']}>Your cart is empty</h3>
                <button className={"btn red waves-effect"} onClick={() => history.push("/")}>Go shopping</button>
              </div>
        }
      </div>)
}

export default memo(Cart);