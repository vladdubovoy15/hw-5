import types from './types';
import localStorageService from "../../localStorageService/localStorageService";

const addItemToCart = payload => ({
  type: types.ADD_TO_CART, payload
})

const removeItemFromCart = payload => ({
  type: types.REMOVE_FROM_CART, payload
})

const setCartFromLocalStorage = () => ({
  type: types.SET_CART, payload: localStorageService.getItem("cart") || []
})

const clearCart = () => ({
  type: types.CLEAR_CART, payload: []
})

const actions = {
  addItemToCart,
  removeItemFromCart,
  setCartFromLocalStorage,
  clearCart
};
export default actions;