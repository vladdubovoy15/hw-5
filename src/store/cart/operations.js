import actions from './actions';
import localStorageService from "../../localStorageService/localStorageService";

const setCart = confirm => (dispatch, getState) => {
  const cart = getState().cart.cart;
  confirm
      ? localStorageService.saveItem('cart', cart)
      : dispatch(actions.setCartFromLocalStorage())
}

const operations = {
  setCart,
  addItemToCart: actions.addItemToCart,
  removeItemFromCart: actions.removeItemFromCart,
  setCartFromLocalStorage: actions.setCartFromLocalStorage,
  clearCart: actions.clearCart,
}

export default operations;