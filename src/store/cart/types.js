const SET_CART = "cart/SET_CART"
const REMOVE_FROM_CART = "cart/REMOVE_FROM_CART"
const ADD_TO_CART = "cart/ADD_TO_CART"
const CLEAR_CART = "cart/CLEAR_CART"

const types = {SET_CART, REMOVE_FROM_CART, ADD_TO_CART, CLEAR_CART};
export default types;