import {combineReducers} from "redux";
import favorite from "./favorite/index";
import cart from "./cart/index";
import app from "./app/index";

const reducer = combineReducers({
  app,
  cart,
  favorite,
})

export default reducer;