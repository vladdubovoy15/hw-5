import axios from "axios";
import actions from "./actions";

const fetchCards = () => async dispatch => {
  const response = await axios.get('/api/phones.json');
  dispatch(actions.saveCards(response.data));
  dispatch(actions.setLoader(false));
}

const operations = {
  fetchCards,
  setLoader: actions.setLoader,
  setModal: actions.setModal,
}
export default operations;