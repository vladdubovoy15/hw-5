const SET_CARDS = "app/SET_CARDS"
const SET_LOADER = "app/SET_LOADER"
const SET_MODAL = "app/SET_MODAL"
const CLOSE_MODAL = "app/CLOSE_MODAL"

const types = {SET_CARDS, SET_LOADER, SET_MODAL, CLOSE_MODAL};
export default types;