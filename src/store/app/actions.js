import types from "./types";

const saveCards = payload => ({type: types.SET_CARDS, payload});

const setLoader = payload => ({type: types.SET_LOADER, payload});

const setModal = payload => ({type: types.SET_MODAL, payload})

const actions = {
  saveCards,
  setLoader,
  setModal,
};

export default actions;