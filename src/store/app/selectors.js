const getModal = () => state => state.app.modal;
const getIsLoading = () => state => state.app.isLoading;
const getCards = () => state => state.app.cards;


const cartSelectors = {getCards, getIsLoading, getModal}
export default cartSelectors;