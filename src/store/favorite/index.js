import favoriteReducer from './favoriteReducer';

export {default as favoriteSelectors} from './selectors';
export {default as favoriteOperations} from './operations';

export default favoriteReducer;