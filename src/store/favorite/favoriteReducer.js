import localStorageService from "../../localStorageService/localStorageService";
import types from "./types";

const initialState = {
  favorite: localStorageService.getItem("favorite") || [],
}

const favoriteReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_FAVORITE: {
      return {...state, favorite: action.payload}
    }
    default:
      return state;
  }
}

export default favoriteReducer;