const getFavorite = () => state => state.favorite.favorite;

const favoriteSelectors = {getFavorite}
export default favoriteSelectors;