import localStorageService from "../../localStorageService/localStorageService";
import types from "./types";

const setFavorite = card => (dispatch, getState) => {
  const favorite = getState().favorite.favorite;

  const filteredFavoriteList = !!favorite.find(item => +item.id === +card.id)
      ? favorite.filter(item => item.id !== card.id)
      : [...favorite, card];
  localStorageService.saveItem('favorite', filteredFavoriteList);

  dispatch({
    type: types.SET_FAVORITE, payload: filteredFavoriteList
  })
}

const operations = {setFavorite}
export default operations;